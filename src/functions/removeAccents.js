function removeAccents(string) {
  var map = {
    a: `á|à|ã|â`,
    A: `À|Á|Ã|Â`,
    e: `é|è|ê`,
    E: `É|È|Ê`,
    i: `í|ì|î`,
    I: `Í|Ì|Î`,
    o: `ó|ò|ô|õ`,
    O: `Ó|Ò|Ô|Õ`,
    u: `ú|ù|û|ü`,
    U: `Ú|Ù|Û|Ü`,
    c: `ç`,
    C: `Ç`,
    n: `ñ`,
    N: `Ñ`
  };

  for (var pattern in map) {
    string = string.replace(new RegExp(map[pattern], `g`), pattern);
  }

  return string;
}

export default removeAccents;
