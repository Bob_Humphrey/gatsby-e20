function destinationTypeName(type) {
  switch (type) {
    case `1`:
      return `Departure/Arrival`;
    case `2`:
      return `Cruise Port`;
    case `3`:
      return `Overnight`;
    case `4`:
      return `Day Trip`;
    default:
      return `Unknown`;
  }
}

export default destinationTypeName;
