function destinationCostName(type) {
  switch (type) {
    case `1`:
      return `Free`;
    case `2`:
      return `Pay`;
    case `3`:
      return `Expensive`;
    case `4`:
      return `Don't Know Cost`;
    default:
      return `Unknown`;
  }
}

export default destinationCostName;
