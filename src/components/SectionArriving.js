import React from "react";

const SectionArriving = props => {
  return (
    <div className="">
      <div
        className="text-justify "
        dangerouslySetInnerHTML={{ __html: props.data.arrival }}
      />
    </div>
  );
};

export default SectionArriving;
