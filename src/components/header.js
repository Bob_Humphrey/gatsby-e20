import { Link } from "gatsby";
import React from "react";

const Header = () => {
  var moment = require(`moment`);
  const today = moment().format(`dddd MMM D`);
  return (
    <header className="flex justify-center bg-primary text-white font-bold font-heading text-4xl text-center py-4">
      <div className="flex justify-between w-11/12 lg:w-2/5">
        <div className="flex justify-start self-center hover:text-primary-light">
          <Link to={`/`}>Europe 2020</Link>
        </div>
        <div className="flex justify-end text-base font-sans font-normal self-center">
          {today}
        </div>
      </div>
    </header>
  );
};

export default Header;
