import React from "react";
import { Link } from "gatsby";

const SectionDayTrips = props => {
  return (
    <div className="">
      {props.data.map(destination => (
        <Link
          className="flex w-full hover:text-tertiary mb-2"
          to={`/destinations/` + destination.directusId}
          key={destination.directusId}
        >
          {destination.name}
        </Link>
      ))}
    </div>
  );
};

export default SectionDayTrips;
