import React from "react";
import Loadable from "react-loadable";
import Copyable from "./Copyable";

function Loading() {
  return <h3>Loading...</h3>;
}

const LoadableCopyable = Loadable({
  render(loaded, props) {
    let Component = loaded.namedExport;
    return <Component {...props} />;
  },
  loading: Loading
});
