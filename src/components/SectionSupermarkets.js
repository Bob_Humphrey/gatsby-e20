import React from "react";
//import Copyable from "./Copyable";

const SectionSupermarkets = props => {
  const { supermarkets } = props.data;
  return (
    <div className="">
      {supermarkets.map(supermarket => (
        <div className="mb-3" key={supermarket.directusId}>
          <div className="font-bold">
            {supermarket.name} - {supermarket.rating}
          </div>
          <div>{supermarket.address}</div>
          <div>{supermarket.description}</div>
        </div>
      ))}
    </div>
  );
};

export default SectionSupermarkets;
