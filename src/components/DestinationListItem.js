import React from "react";
import { Link } from "gatsby";
import destinationTypeName from "../functions/destinationTypeName.js";

const DestinationListItem = props => {
  return (
    <Link
      className="flex w-full hover:text-secondary mb-3"
      to={`/destinations/` + props.destination.directusId}
    >
      <div className="md:w-1/5"></div>
      <div className="w-1/3 md:w-1/5">{props.destination.arrival_date}</div>
      <div className="w-1/3 md:w-1/5">{props.destination.name}</div>
      <div className="w-1/3 md:w-1/5">
        {destinationTypeName(props.destination.type)}
      </div>
      <div className="md:w-1/5"></div>
    </Link>
  );
};

export default DestinationListItem;
