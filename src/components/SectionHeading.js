import React from "react";
import destinationTypeName from "../functions/destinationTypeName.js";
import DestinationProperty from "./DestinationProperty.js";
import removeAccents from "../functions/removeAccents";

const SectionHeading = props => {
  const cruiseArrivalTime = props.data.cruise_arrival_time
    ? `  ` + props.data.cruise_arrival_time.substring(0, 5)
    : ``;
  const cruiseDepartureTime = props.data.cruise_departure_time
    ? `  ` + props.data.cruise_departure_time.substring(0, 5)
    : ``;
  return (
    <div>
      <h2 className="font-heading text-secondary text-3xl font-bold mb-4 border-b border-secondary">
        {removeAccents(props.data.name)}
      </h2>
      <DestinationProperty
        label={destinationTypeName(props.data.type)}
        value=""
      />
      <DestinationProperty
        label="Arrival"
        value={props.data.arrival_date + cruiseArrivalTime}
      />
      <DestinationProperty
        label="Departure"
        value={props.data.departure_date + cruiseDepartureTime}
      />
    </div>
  );
};

export default SectionHeading;
