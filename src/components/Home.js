import React from "react";
import { StaticQuery, graphql } from "gatsby";
import DestinationListItem from "./DestinationListItem.js";

const DESTINATION_LIST = graphql`
  query DestinationList {
    allDirectusDestination(
      sort: { fields: arrival_date }
      filter: { arrival_date: { ne: null } }
    ) {
      edges {
        node {
          directusId
          arrival_date(formatString: "ddd MMM D")
          type
          name
        }
      }
    }
  }
`;

const Home = () => {
  return (
    <div>
      <div className="flex justify-center w-full mt-10 mb-6">
        <div className="w-11/12 lg:w-1/2">
          <div className="flex flex-wrap">
            <StaticQuery
              query={DESTINATION_LIST}
              render={({ allDirectusDestination }) =>
                allDirectusDestination.edges.map(({ node }) => (
                  <DestinationListItem
                    destination={node}
                    key={node.directusId}
                  />
                ))
              }
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
