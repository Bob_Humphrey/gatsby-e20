import React from "react";
//import Copyable from "./Copyable";

const DestinationPropertyCopyable = props => {
  return (
    <div className="md:flex w-full mb-1">
      <div className="w-full md:w-1/5 font-bold md:text-right mr-6">
        {props.label}
      </div>
      <div className="w-full md:w-4/5">{props.value}</div>
    </div>
  );
};

export default DestinationPropertyCopyable;
