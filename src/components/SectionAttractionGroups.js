import React from "react";
import AttractionDetail from "./AttractionDetail";

const SectionAttractionGroups = props => {
  return (
    <div>
      <div
        className="text-justify "
        dangerouslySetInnerHTML={{ __html: props.data.notes }}
      />
      {props.data.attractions.map(attraction => (
        <AttractionDetail data={attraction} key={attraction.directusId} />
      ))}
    </div>
  );
};

export default SectionAttractionGroups;
