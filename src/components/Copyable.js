import React from "react";
import useClippy from "use-clippy";

const Copyable = props => {
  const [clipboard, setClipboard] = useClippy();
  return (
    <div
      className=""
      onClick={() => {
        setClipboard(props.data);
      }}
    >
      <span className="border-b border-dashed border-tertiary">
        {props.data}
      </span>
    </div>
  );
};

export default Copyable;
