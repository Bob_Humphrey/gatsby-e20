import React from "react";

const DestinationProperty = props => {
  return (
    <div className="md:flex w-full mb-1">
      <div className="w-full md:w-1/5 font-bold md:text-right mr-6">
        {props.label}
      </div>
      <div className="w-full md:w-4/5 text-justify">{props.value}</div>
    </div>
  );
};

export default DestinationProperty;
