import React from "react";

const DestinationPropertyRaw = props => {
  return (
    <div className="md:flex w-full mb-1">
      <div className="w-full md:w-1/5 font-bold md:text-right mr-6">
        {props.label}
      </div>
      <div
        className="w-full md:w-4/5 text-justify "
        dangerouslySetInnerHTML={{ __html: props.value }}
      />
    </div>
  );
};

export default DestinationPropertyRaw;
