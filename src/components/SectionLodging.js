import React from "react";
import DestinationProperty from "./DestinationProperty";
import DestinationPropertyRaw from "./DestinationPropertyRaw";
import DestinationPropertyCopyable from "./DestinationPropertyCopyable";

const SectionLodging = props => {
  const { data } = props;
  const links =
    `<a
      href=` +
    data.lodging_listing +
    ` target="_blank"
      rel="noopener noreferrer"
      class="hover:text-tertiary"
    >
      Listing
    </a>
    &nbsp; / &nbsp;
    <a
      href=` +
    data.lodging_reservation +
    ` target="_blank"
      rel="noopener noreferrer"
      class="hover:text-tertiary"
    >
      Reservation
    </a>
    &nbsp; / &nbsp;
    <a
      href=` +
    data.lodging_host_message +
    ` target="_blank"
      rel="noopener noreferrer"
      class="hover:text-tertiary"
    >
      Message Hosts
    </a>`;
  const washerYes = `<div class="w-6"><svg 
      xmlns="http://www.w3.org/2000/svg" 
      viewBox="0 0 20 20">
      <path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM6.7 9.29L9 11.6l4.3-4.3 1.4 1.42L9 14.4l-3.7-3.7 1.4-1.42z"/>
      </svg></div>`;
  const washerNo = `<div class="w-6"><svg 
      xmlns="http://www.w3.org/2000/svg" 
      viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm1.41-1.41A8 8 0 1 0 15.66 4.34 8 8 0 0 0 4.34 15.66zm9.9-8.49L11.41 10l2.83 2.83-1.41 1.41L10 11.41l-2.83 2.83-1.41-1.41L8.59 10 5.76 7.17l1.41-1.41L10 8.59l2.83-2.83 1.41 1.41z"/>
      </svg></div>`;
  const washer = data.lodging_washer ? washerYes : washerNo;
  return (
    <div className="">
      <DestinationProperty label="Checkin" value={data.lodging_checkin} />
      <DestinationProperty label="Checkout" value={data.lodging_checkout} />
      <DestinationProperty label="Host(s)" value={data.lodging_hosts} />
      <DestinationPropertyRaw label="Links" value={links} />
      <DestinationPropertyCopyable
        label="Address"
        value={data.lodging_address}
      />
      <DestinationPropertyRaw
        label="Checkin Inst"
        value={data.lodging_checkin_instructions}
      />
      <DestinationPropertyRaw
        label="Directions"
        value={data.lodging_arrival_directions}
      />
      <DestinationPropertyRaw label="Notes" value={data.lodging_notes} />
      <DestinationPropertyRaw label="Washer" value={washer} />
    </div>
  );
};

export default SectionLodging;
