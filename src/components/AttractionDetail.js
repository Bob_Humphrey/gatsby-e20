import React from "react";
import destinationCostName from "../functions/destinationCostName";
//import Copyable from "./Copyable";

const AttractionDetail = props => {
  const description = props.data.description ? props.data.description : ``;
  const starIcon = `<svg class="self-end h-8 w-8 fill-current mr-2"  
    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
    <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/>
    </svg>`;
  const star = props.data.priority ? starIcon : ``;
  const cost = destinationCostName(props.data.cost);
  return (
    <div className="">
      <div className="flex text-2xl text-tertiary mt-10 mb-2">
        <span className="" dangerouslySetInnerHTML={{ __html: star }}></span>
        <div className="">{props.data.name}</div>
      </div>
      <div className="text-sm uppercase">{cost}</div>
      <div className="font-bold text-justify mt-2 mb-2 ">{description}</div>
      {props.data.attraction_notes.map(note => (
        <div className="mb-4" key={note.directusId}>
          <div className="text-justify mb-2">{note.description}</div>
          <a
            className="text-sm uppercase text-gray-500"
            href={note.source.web_url}
            target="_blank"
            rel="noopener noreferrer"
          >
            {note.source.website} - {note.source.title}
          </a>
        </div>
      ))}
    </div>
  );
};

export default AttractionDetail;
