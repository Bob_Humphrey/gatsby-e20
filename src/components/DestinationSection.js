import React, { useState } from "react";
import SectionLodging from "./SectionLodging";
import SectionArriving from "./SectionArriving";
import SectionSupermarkets from "./SectionSupermarkets";
import SectionLocalTransportation from "./SectionLocalTransportation";
import SectionRestaurants from "./SectionRestaurants";
import SectionDayTrips from "./SectionDayTrips";
import SectionAttractionGroups from "./SectionAttractionGroups";
import SectionReference from "./SectionReference";
import SectionNotes from "./SectionNotes";

const DestinationSection = props => {
  const openIcon = `<svg class="h-10 w-10 fill-current self-end pb-2" 
    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
    <path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm5-9v2H5V9h10z"/>
    </svg>`;
  const closeIcon = `<svg class="h-10 w-10 fill-current self-end pb-2"  
    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
    <path d="M11 9h4v2h-4v4H9v-4H5V9h4V5h2v4zm-1 11a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16z"/>
    </svg>`;
  const [showSection, setShowSection] = useState(false);
  const sectionIcon = showSection ? closeIcon : openIcon;
  const sectionClassName = showSection ? `` : `hidden`;
  return (
    <div>
      <div
        className="flex justify-between font-heading text-tertiary text-3xl font-bold mt-4 mb-4 border-b border-tertiary"
        onClick={() => {
          setShowSection(!showSection);
        }}
      >
        <div className="flex justify-start">{props.name}</div>
        <div
          className="flex justify-end w-16"
          dangerouslySetInnerHTML={{ __html: sectionIcon }}
        ></div>
      </div>

      <div className={sectionClassName}>
        {props.type === `arriving` && <SectionArriving data={props.data} />}
        {props.type === `lodging` && <SectionLodging data={props.data} />}
        {props.type === `supermarkets` && (
          <SectionSupermarkets data={props.data} />
        )}
        {props.type === `local transportation` && (
          <SectionLocalTransportation data={props.data} />
        )}
        {props.type === `restaurants` && (
          <SectionRestaurants data={props.data} />
        )}
        {props.type === `day trips` && <SectionDayTrips data={props.data} />}
        {props.type === `attraction groups` && (
          <SectionAttractionGroups data={props.data} />
        )}
        {props.type === `reference` && <SectionReference data={props.data} />}
        {props.type === `notes` && <SectionNotes data={props.data} />}
      </div>
    </div>
  );
};

export default DestinationSection;
