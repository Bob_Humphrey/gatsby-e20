import React from "react";
//import Copyable from "./Copyable";

const SectionRestaurants = props => {
  const { restaurants } = props.data;
  return (
    <div className="">
      {restaurants.map(restaurant => (
        <div className="mb-3" key={restaurant.directusId}>
          <div className="font-bold">{restaurant.name}</div>
          <div>{restaurant.address}</div>
          <div
            className="text-justify"
            dangerouslySetInnerHTML={{ __html: restaurant.notes }}
          ></div>
        </div>
      ))}
    </div>
  );
};

export default SectionRestaurants;
