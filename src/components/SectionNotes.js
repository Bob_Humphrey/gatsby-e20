import React from "react";

const SectionNotes = props => {
  return (
    <div>
      <div
        className="text-justify"
        dangerouslySetInnerHTML={{ __html: props.data.notes }}
      ></div>
      {props.data.destination_notes.map(note => (
        <div className="mb-4" key={note.directusId}>
          <div className="text-justify mb-2">{note.description}</div>
          <a
            className="text-sm uppercase text-gray-500"
            href={note.source.web_url}
            target="_blank"
            rel="noopener noreferrer"
          >
            {note.source.website} - {note.source.title}
          </a>
        </div>
      ))}
    </div>
  );
};

export default SectionNotes;
