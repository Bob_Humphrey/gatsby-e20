import React from "react";

const SectionReference = props => {
  return (
    <div className="">
      {props.data.map(source => (
        <div className="mb-3" key={source.directusId}>
          <a
            className="hover:text-tertiary"
            href={source.web_url}
            target="_blank"
            rel="noopener noreferrer"
          >
            {source.website} - {source.title}
          </a>
        </div>
      ))}
    </div>
  );
};

export default SectionReference;
