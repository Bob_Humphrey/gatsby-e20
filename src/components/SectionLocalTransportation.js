import React from "react";

const SectionLocalTransportation = props => {
  return (
    <div className="">
      <div
        className="text-justify "
        dangerouslySetInnerHTML={{ __html: props.data.local_transportation }}
      />
    </div>
  );
};

export default SectionLocalTransportation;
