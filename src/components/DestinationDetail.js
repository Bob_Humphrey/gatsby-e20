import React from "react";
import { graphql } from "gatsby";
import Layout from "./layout";
import SectionHeading from "./SectionHeading";
import DestinationSection from "./DestinationSection.js";

export const query = graphql`
  query DestinationQuery($directusId: Int!) {
    directusDestination(directusId: { eq: $directusId }) {
      directusId
      name
      type
      arrival_date(formatString: "dddd MMM D")
      departure_date(formatString: "dddd MMM D")
      cruise_arrival_time
      cruise_departure_time
      arrival
      lodging_address
      lodging_arrival_directions
      lodging_checkin
      lodging_checkin_instructions
      lodging_checkout
      lodging_listing
      lodging_notes
      lodging_reservation
      lodging_hosts
      lodging_host_message
      lodging_washer
      local_transportation
      notes
      destination_notes {
        directusId
        description
        source {
          directusId
          author
          title
          web_url
          website
        }
      }
      day_trips_to {
        directusId
        name
      }
      attraction_groups {
        directusId
        name
        notes
        attractions {
          directusId
          name
          cost
          priority
          description
          attraction_notes {
            directusId
            description
            source {
              author
              title
              web_url
              website
            }
          }
        }
      }
      sources {
        directusId
        title
        web_url
        website
      }
      supermarkets {
        name
        directusId
        address
        rating
        description
      }
      restaurants {
        directusId
        name
        address
        notes
      }
    }
  }
`;

const DestinationDetail = props => {
  const { directusDestination } = props.data;
  return (
    <Layout>
      <article className="flex justify-center w-full mt-10 mb-6">
        <div className="w-11/12 lg:w-2/5">
          {/* HEADING */}
          <SectionHeading data={directusDestination} key="A000" />

          {/* ARRIVING */}
          <DestinationSection
            type="arriving"
            name="Arriving"
            data={directusDestination}
            key="A050"
          />

          {/* LODGING */}
          {directusDestination.type === `3` && (
            <DestinationSection
              type="lodging"
              name="Lodging"
              data={directusDestination}
              key="A100"
            />
          )}

          {/* SUPERMARKETS */}
          {directusDestination.type === `3` && (
            <DestinationSection
              type="supermarkets"
              name="Supermarkets"
              data={directusDestination}
              key="A130"
            />
          )}

          {/* LOCAL TRANSPORTATION */}
          {directusDestination.type === `3` && (
            <DestinationSection
              type="local transportation"
              name="Local Transportation"
              data={directusDestination}
              key="A150"
            />
          )}

          {/* RESTAURANTS */}
          {directusDestination.type === `3` && (
            <DestinationSection
              type="restaurants"
              name="Restaurants"
              data={directusDestination}
              key="A160"
            />
          )}

          {/* ATTRACTION GROUPS */}
          {directusDestination.attraction_groups.map(group => (
            <DestinationSection
              type="attraction groups"
              name={group.name}
              data={group}
              key={group.directusId}
            />
          ))}

          {/* DAY TRIPS */}
          {directusDestination.type === `3` && (
            <DestinationSection
              type="day trips"
              name="Day Trips"
              data={directusDestination.day_trips_to}
              key="A200"
            />
          )}

          {/* NOTES */}
          <DestinationSection
            type="notes"
            name="Notes"
            data={directusDestination}
            key="A300"
          />

          {/* REFERENCE */}
          <DestinationSection
            type="reference"
            name="Reference"
            data={directusDestination.sources}
            key="A400"
          />
        </div>
      </article>
    </Layout>
  );
};

export default DestinationDetail;
