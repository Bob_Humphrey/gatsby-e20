import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Home from "../components/Home";

function IndexPage() {
  return (
    <Layout>
      <SEO keywords={[`europe`, `vacation`]} title="Europe 2020" />
      <Home />
    </Layout>
  );
}

export default IndexPage;
