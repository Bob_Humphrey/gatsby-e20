const path = require(`path`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  return new Promise((resolve, reject) => {
    graphql(`
      {
        allDirectusDestination(sort: { fields: directusId }) {
          edges {
            node {
              directusId
              name
              type
              arrival_date(formatString: "dddd MMM D")
              departure_date(formatString: "dddd MMM D")
              cruise_departure_time
              cruise_arrival_time
              lodging_address
              lodging_arrival_directions
              lodging_checkin
              lodging_checkin_instructions
              lodging_checkout
              lodging_listing
              lodging_notes
              lodging_reservation
              lodging_washer
            }
          }
        }
      }
    `).then(results => {
      results.data.allDirectusDestination.edges.forEach(({ node }) => {
        createPage({
          path: `/destinations/${node.directusId}`,
          component: path.resolve(`./src/components/DestinationDetail.js`),
          context: {
            directusId: node.directusId
          }
        });
      });
      resolve();
    });
  });
};
